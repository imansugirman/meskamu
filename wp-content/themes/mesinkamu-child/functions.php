<?php
// Add custom Theme Functions here
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'mesinkamu-style', get_stylesheet_directory_uri() . '/style.css');
    wp_enqueue_style( 'mesinkamu-custom', get_stylesheet_directory_uri() . '/css/customproxima.css');
    wp_enqueue_style( 'mesinkamu-font', get_stylesheet_directory_uri() . '/css/font.css');
    // wp_enqueue_style( 'inacookies-gotham', get_stylesheet_directory_uri() . '/css/gotham.css');
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['reviews']['title'] = __( 'Ratings' );				// Rename the reviews tab
	$tabs['additional_information']['title'] = __( 'Spesification' );	// Rename the additional information tab

	return $tabs;

}